<?php

require_once ('frog.php');
require_once ('ape.php');

$frog1 = new frog ("Shaun");
echo "Nama = " . $frog1->animal . "<br>";
echo "Legs = " . $frog1 ->legs . "<br>";
echo "Cold Bloded = " . $frog1->coldbloded . "<br> <br>";

$ape1 = new ape ("buduk");
echo "Nama = " . $ape1->animal . "<br>";
echo "Legs= " . $ape1->legs . "<br>";
echo "Cold Bloded = " . $ape1->coldbloded . "<br>";
$ape1->jump('hop');


$kera1 = new kera ("Kera sakti");
echo "Nama = " . $kera1->animal . "<br>";
echo "Legs= " . $kera1->legs . "<br>";
echo "Cold Bloded = " . $kera1->coldbloded . "<br>";
$kera1->yell('Auooo');